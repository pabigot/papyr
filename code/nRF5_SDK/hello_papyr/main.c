/** @file main.c
 *
 * @brief Hello_Papyr test code with nRF5 SDk. 
 *
 * @par
 * COPYRIGHT NOTICE: (c) 2018 Electronut Labs.
 * All rights reserved. 
*/

#include "nrf_drv_spi.h"
#include "app_util_platform.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "boards.h"
#include "app_error.h"
#include <string.h>
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "nrf_pwr_mgmt.h"

#include "epdif.h"
#include "epaper.h"

#define PIN_E_INK 11
#define GREEN_LED 13

/* initialise SPI for epaper pins */
EPAPER_pins epaper_pins = {
    .pinBUSY = 3,
    .pinRST = 2,
    .pinDC = 28,
    .pinSpiCS = 30,
    .pinSpiSCK = 31,
    .pinSpiMOSI = 29,
};

/**
 *  @brief: turn on epaper
 */
void epaper_turnOn(void)
{
    // turn on epaper mosfet
    nrf_gpio_cfg_output(PIN_E_INK);
    nrf_gpio_pin_clear(PIN_E_INK);
}

/**
 *  @brief: initialise led and blink green
 */
void led_init(void)
{
    nrf_gpio_cfg_output(GREEN_LED);
    nrf_gpio_pin_clear(GREEN_LED);
    nrf_delay_ms(500);
    nrf_gpio_pin_set(GREEN_LED);
    nrf_delay_ms(500);
}

// Display default image
void display_hello_papyr()
{
    Paint_Init(&paint_black, frame_buffer_black, epd.width, epd.height);
    Paint_Init(&paint_red, frame_buffer_red, epd.width, epd.height);
    Paint_Clear(&paint_black, UNCOLORED);
    Paint_Clear(&paint_red, UNCOLORED);
    
    // Display hello Papyr!
    Paint_SetRotate(&paint_black, 3);
    Paint_DrawStringAt(&paint_black, 20, 100, "Hello Papyr!", &Font20, COLORED);

    EPD_DisplayFrame(&epd, frame_buffer_black, frame_buffer_red);
}

/**
 *  @brief: main
 */
int main(void)
{
    bsp_board_init(BSP_INIT_LEDS);

    APP_ERROR_CHECK(NRF_LOG_INIT(NULL));
    NRF_LOG_DEFAULT_BACKENDS_INIT();

    NRF_LOG_INFO("Displaying default image...");
    NRF_LOG_FLUSH();

    epaper_turnOn();

    led_init();

    epaper_init();
    display_hello_papyr();

    NRF_LOG_INFO("DONE!");
    NRF_LOG_FLUSH();

    while (1)
    {
       /* empty loop */
    }
}
