## Web Bluetooth Demo

The index.js file is used by main README.md file of the repository, and is consumed by mkdocs for Web Bluetooth demo on main docs page.
To see the demo, visit this [link](https://docs.electronut.in/papyr/#try-it-here).