#pragma once

// temperature/humidity data
// data is of the form
// 0:123.4:567.8
typedef struct _th_data 
{
	char id;
	char T[6];
	char H[6];
} th_data;

