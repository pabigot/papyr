/* udp.c - UDP specific code for echo client */

/*
 * Copyright (c) 2017 Intel Corporation.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <logging/log.h>
LOG_MODULE_DECLARE(dht11_client_sample, LOG_LEVEL_DBG);

#include <zephyr.h>
#include <errno.h>
#include <stdio.h>

#include <net/net_pkt.h>
#include <net/net_core.h>
#include <net/net_context.h>

#include <net/net_app.h>

#include "common.h"

static struct net_app_ctx udp6;
extern char data[];

#define UDP_SLEEP K_MSEC(150)

#define BUF_TIMEOUT K_MSEC(100)


/* Note that both tcp and udp can share the same pool but in this
 * example the UDP context and TCP context have separate pools.
 */
#if defined(CONFIG_NET_CONTEXT_NET_PKT_POOL)
NET_PKT_TX_SLAB_DEFINE(echo_tx_udp, 5);
NET_PKT_DATA_POOL_DEFINE(echo_data_udp, 20);

static struct k_mem_slab *tx_udp_slab(void)
{
	return &echo_tx_udp;
}

static struct net_buf_pool *data_udp_pool(void)
{
	return &echo_data_udp;
}
#else
#define tx_udp_slab NULL
#define data_udp_pool NULL
#endif /* CONFIG_NET_CONTEXT_NET_PKT_POOL */


#define dtls_result_ipv6 NULL
#define dtls_result_ipv4 NULL
#define net_app_dtls_stack_ipv4 NULL
#define net_app_dtls_stack_ipv6 NULL


struct net_pkt *prepare_send_pkt(struct net_app_ctx *ctx,
				 const char *name,
				 int *expecting_len)
{
	struct net_pkt *send_pkt;

	send_pkt = net_app_get_net_pkt(ctx, AF_UNSPEC, BUF_TIMEOUT);
	if (!send_pkt) {
		return NULL;
	}

	*expecting_len = net_pkt_append(send_pkt, *expecting_len, data,
					K_FOREVER);

	return send_pkt;
}

static void send_udp_data(struct net_app_ctx *ctx, struct data *data)
{
	struct net_pkt *pkt;
	size_t len;
	int ret;

	data->expecting_udp = 13;

	pkt = prepare_send_pkt(ctx, data->proto, &data->expecting_udp);
	if (!pkt) {
		return;
	}

	len = net_pkt_get_len(pkt);

	NET_ASSERT_INFO(data->expecting_udp == len,
			"Data to send %d bytes, real len %zu",
			data->expecting_udp, len);

	ret = net_app_send_pkt(ctx, pkt, NULL, 0, K_FOREVER,
			       UINT_TO_POINTER(len));
	if (ret < 0) {
		LOG_ERR("Cannot send %s data to peer (%d)", data->proto, ret);

		net_pkt_unref(pkt);
	}

	k_delayed_work_submit(&data->recv, WAIT_TIME);
}


/* We can start to send data when UDP is "connected" */
static void udp_connected(struct net_app_ctx *ctx,
			  int status,
			  void *user_data)
{
	struct data *data = user_data;

	data->udp = ctx;

	send_udp_data(ctx, data);
}

void send_data()
{
	struct net_app_ctx *ctx = &udp6;
	struct data *data = ctx->user_data;
	data->udp = ctx;

	send_udp_data(ctx, data);
}

static int connect_udp(struct net_app_ctx *ctx, const char *peer,
		       void *user_data, u8_t *dtls_result_buf,
		       size_t dtls_result_buf_len,
		       k_thread_stack_t *stack, size_t stack_size)
{
	struct data *data = user_data;
	int ret;

	ret = net_app_init_udp_client(ctx, NULL, NULL, peer, PEER_PORT,
				      WAIT_TIME, user_data);
	if (ret < 0) {
		LOG_ERR("Cannot init %s UDP client (%d)", data->proto, ret);
		goto fail;
	}

#if defined(CONFIG_NET_CONTEXT_NET_PKT_POOL)
	net_app_set_net_pkt_pool(ctx, tx_udp_slab, data_udp_pool);
#endif

	ret = net_app_set_cb(ctx, udp_connected, NULL, NULL, NULL);
	if (ret < 0) {
		LOG_ERR("Cannot set callbacks (%d)", ret);
		goto fail;
	}

	ARG_UNUSED(dtls_result_buf);
	ARG_UNUSED(dtls_result_buf_len);
	ARG_UNUSED(stack);
	ARG_UNUSED(stack_size);

	ret = net_app_connect(ctx, CONNECT_TIME);
	if (ret < 0) {
		LOG_ERR("Cannot connect UDP (%d)", ret);
		goto fail;
	}

fail:
	return ret;
}

static void wait_reply(struct k_work *work)
{
	/* This means that we did not receive response in time. */
	struct data *data = CONTAINER_OF(work, struct data, recv);

	LOG_ERR("Data packet not received");

	/* Send a new packet at this point */
	send_udp_data(data->udp, data);
}

void start_udp(void)
{
	int ret;

	if (IS_ENABLED(CONFIG_NET_IPV6)) {
		k_delayed_work_init(&conf.ipv6.recv, wait_reply);

		ret = connect_udp(&udp6, CONFIG_NET_CONFIG_PEER_IPV6_ADDR,
				  &conf.ipv6, dtls_result_ipv6,
				  sizeof(dtls_result_ipv6),
				  net_app_dtls_stack_ipv6,
				  K_THREAD_STACK_SIZEOF(
					  net_app_dtls_stack_ipv6));
		if (ret < 0) {
			LOG_ERR("Cannot init IPv6 UDP client (%d)", ret);
		}
	}

}

void stop_udp(void)
{
	if (IS_ENABLED(CONFIG_NET_IPV6)) {
		net_app_close(&udp6);
		net_app_release(&udp6);
	}
}
