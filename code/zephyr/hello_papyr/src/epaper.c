/** @file epaper.c
 *
 * @brief epaper driver for zephyr. 
 *
 * @par
 * COPYRIGHT NOTICE: (c) 2018 Electronut Labs.
 * All rights reserved. 
*/

#include "epaper.h"

#include "epaper_driver/epd1in54b.h"
#include "epaper_driver/epdif.h"
#include "epaper_driver/epdpaint.h"
#include "epaper_driver/image.h"
#include "epaper_driver/fonts.h"

EPD epd; // declare epaper instance

unsigned char frame_buffer_black_arr[EPD_WIDTH * EPD_HEIGHT / 8];
unsigned char *frame_buffer_black = frame_buffer_black_arr;
unsigned char frame_buffer_red_arr[EPD_WIDTH * EPD_HEIGHT / 8];
unsigned char *frame_buffer_red = frame_buffer_red_arr;

Paint paint_black;
Paint paint_red;

/**
 *  @brief: initialise epaper
 */
int epaper_init()
{
    epaper_GPIO_Init();

    return EPD_Init(&epd);
}
/**
 *  @brief: display Hello Papyr! on epaper
 */
void epaper_test()
{
    Paint_Init(&paint_black, frame_buffer_black, epd.width, epd.height);
    Paint_Init(&paint_red, frame_buffer_red, epd.width, epd.height);
    Paint_Clear(&paint_black, UNCOLORED);
    Paint_Clear(&paint_red, UNCOLORED);

    // Display hello Papyr!
    Paint_SetRotate(&paint_black, 3);
    Paint_DrawStringAt(&paint_black, 20, 100, "Hello Papyr!", &Font20, COLORED);

    EPD_DisplayFrame(&epd, frame_buffer_black, frame_buffer_red);
}
/**
 *  @brief: clear epaper
 */
void epaper_clear()
{
    Paint_Init(&paint_black, frame_buffer_black, epd.width, epd.height);
    Paint_Init(&paint_red, frame_buffer_red, epd.width, epd.height);
    Paint_Clear(&paint_black, UNCOLORED);
    Paint_Clear(&paint_red, UNCOLORED);
    EPD_DisplayFrame(&epd, frame_buffer_black, frame_buffer_red);
}