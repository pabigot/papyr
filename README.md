![Papyr](papyr1.jpg)

## What is Papyr?

With the exploding number of connected devices being deployed, power consumption is a major concern. Technologies like BLE (Bluetooth Low Energy) are built from the ground up with low power consumption in mind. Another technology which is extremely low power is the e-paper display, which was made famous by its adoption by Amazon for the Kindle devices. Papyr from Electronut labs combines these two core ideas - a low power wireless technology combined with a low power display system. By choosing the Nordic nRF52840 SoC, Papyr is able to support not only BLE but mesh networking protocols like Thread, BLE Mesh, and Zigbee. Papyr has many extras too - like build in NFC antenna for BLE pairing or Thread commisioning, CR2477 battery holder, micro USB port, extra GPIOs, RGB LED, etc. There are a lot of applications possible with Papyr - dynamic price tags, for example, or sensor data display in a mesh network. Papyr can be used anywhere you need a low power, connected display.

## Getting Started 

Thank you for purchasing **Papyr**, the compact Nordic nRF52840 based epaper 
display developed by Electronut Labs.

Here's how you can get started with Papyr.

## Hardware Setup

Insert a CR2477 coin cell into the holder, or connect power via the micro USB 
connector. Make sure S2 is set to the correct position - USB or BATT.

**!!!WARNING!!!**

**Ensure correct coin cell polarity before inserting it. Papyr does not have reverse polarity protection, and will get damaged if battery is inserted reversed.**

On start, the LED will blink GREEN twice and turn off. At this point, Papyr is 
advertising as a BLE peripheral and you can connect to it from the Electronut Labs mobile 
app. 


## Using Web Bluetooth

You can talk directly to Papyr using [Web Bluetooth](https://webbluetoothcg.github.io/web-bluetooth/). For this to work, your browser must support Web Bluetooth. To check list of available browsers, visit this [link](https://developer.mozilla.org/en-US/docs/Web/API/Web_Bluetooth_API#Browser_compatibility). On Google Chrome, you should have enabled experimental web features by visiting the url :
```chrome://flags/#enable-experimental-web-platform-features```

Please watch this video use web-bluetooth on your Papyr device.

<iframe width="800" height="500" src="https://www.youtube.com/embed/LVxTsgLguQ8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


### Try it here !
 
<p id="status_p"></p>
<button data-md-color-primary="light-blue" id="connect">Connect</button>
<button data-md-color-primary="light-blue"  id="send">Send</button>
</br>
<div id="attrib1" style="display:flex;">
Pen Color : <p style="background-color:white;border:1px solid black; width: 40px; height:40px;margin: 0px 20px 0px 20px;" onclick="selectColor('white')" ></p>
<p style="background-color:red;border:1px solid black; width: 40px; height:40px;margin: 0px 20px 0px 0px;" onclick="selectColor('red')" ></p>
<p style="background-color:black;border:1px solid black; width: 40px; height:40px;margin: 0px 20px 0px 0px;" onclick="selectColor('black')" > </p>
</div>
</br>
<div id="attrib2" style="display:flex;">
Bkg Color : <p style="background-color:white;border:1px solid black; width: 40px; height:40px;margin: 0px 20px 0px 20px;" onclick="selectbgColor('white')" ></p>
<p style="background-color:red;border:1px solid black; width: 40px; height:40px;margin: 0px 20px 0px 0px;" onclick="selectbgColor('red')" ></p>
<p style="background-color:black;border:1px solid black; width: 40px; height:40px;margin: 0px 20px 0px 0px;" onclick="selectbgColor('black')" > </p>
</div>
</br>
<canvas style="border:1px solid black;" id="canvas1" #imageCanvas ontouchstart="startDrawing(event)" ontouchmove="moved(event)" ontouchend="stopDrawing(event)" onmousedown="startDrawing(event)" onmousemove="moved(event)" onmouseup="stopDrawing(event)"></canvas>
<canvas style="display: none;" id="canvas2" width="200" height="200"></canvas>
<script src="./code/web-bluetooth-demo/index.js"></script>

## Using the Mobile App 

Download the Electronut Labs mobile app :

* [Android Play store](https://play.google.com/store/apps/details?id=in.electronut.app). 
* [iOS App store](https://itunes.apple.com/app/electronut-labs/id1455175273)
 
Here is a demo [video](https://www.youtube.com/watch?v=qC32B0Vj5Lw) of the Electronut Labs mobile app (iOS) interacting with Papyr.


Click on *Papyr*, and you will should find your Papyr device in the list of 
scanned BLE peripherals. Click on the Papyr entry and you will see the following 
dialog. 

There are three options on application - Draw, Select image, Trasnfer data over MQTT.

### Draw on Phone

![papyr app](app1.png)

As shown above, draw something on the screen, and hit "Send Image" when done. The 
LED on Papyr will blink BLE as the image is transferred, and in less than 30 
seconds, your image will appear on Papyr.

![papyr drawing](papyr2.jpg)

### Select Image from Device

![papyr local image](app2.jpg)

Second way to connect to papyr using application is by selecting a file already present on your phone. After selecting the option, you will get a screen that asks to select an image from your gallery. After selecting, change the threshold depending on the closeness of color to red and black. After that, just hit the "Send" button to transfer the processed image to papyr display. 

![papyr local image](papyr3.jpg)

### Transfer Image Data over MQTT

Third mode of image transfer to papyr involves transfer of image over MQTT. You need internet connection on your mobile device to connect to a broker. 

![papyr local image](mqtt_all.png)

* Select "Send Data over MQTT" on clicking the papyr device from the scanned list.
* Click MQTT Config to connect app to MQTT over Websockets. Default to app are linked to hivemq's mqtt-dashboard with "hostname : broker.mqttdashboard.com" and "port : 8000". Tap "Connect" button to connect app to mqtt broker.
* Once connected, you will get the topics to connect to. You can use either a MQTT web client, like [http://www.hivemq.com/demos/websocket-client/](http://www.hivemq.com/demos/websocket-client/) , and connect to MQTT broker mentioned above.
* On the HiveMQ web client, connect to same host. Set topic name to one mentioned on the app. You can send two types of data:
    * SVG - create a svg using [online tools](https://www.rapidtables.com/web/tools/svg-viewer-editor.html) 
    * Base64 - Use [online tool](http://b64.io/) to convert image to base64.
    * Example : 

```xml 
    <svg xmlns="http://www.w3.org/2000/svg" width="200" height="200" version="1.1">
        <g>
            <rect width="200" height="100" stroke="black" stroke-width="0" fill="red" />
            <text x="10" y="80" font-size="30" stroke="white" stroke-width="0" fill="white">Electronut</text>
            <circle cx="170" cy="50" r="10" fill="white" />
            <circle cx="170" cy="20" r="10" fill="black" />
        </g>
        <g>
            <rect y="100" width="200" height="100" stroke="black" stroke-width="0" fill="black" />
            <text x="10" y="120" stroke="white" stroke-width="0" font-size="20" fill="white">Labs</text>
            <circle cx="170" cy="150" r="10" fill="white" />
            <circle cx="170" cy="180" r="10" fill="red" />
        </g>
    </svg>

```
* After publishing content from hive's web client, check the threshold on app for both red and black color and hit "Send" to display the image on papyr display.

![papyr mqtt image](papyr4.jpg)

## Factory Reset

When running the default firmware, once you transfer an image to Papyr, it will 
remain there across power resets. This is by design. If you want to go back to 
the original image (papyr + Electronut Labs logo), there's a little trick - 
connect GPIO P0.08 to VDD and press S1 (reset), and boom - the original image 
is restored.

## Programming Papyr

**!!!WARNING!!!**

**There is a small error in the labels of the Papyr v0.3 PCB SWD header. 
Hence they have been blacked out. See image below for correct labels.**

![SWD pins](papyr-swd-corr.jpg)

Papyr can be reprogrammed using SWD header. You can use our inexpensive [Bumpy](https://docs.electronut.in/bumpy/) SWD debugger and [PogoProg Model B](https://docs.electronut.in/PogoProg/) adapter for that purpose. 

![papyr programming](papyr-prog.jpg)

Information regarding programming Papyr can be found [here](https://docs.electronut.in/papyr/programming_guide/).

You can find several interesting applications of Papyr in the *code* directory.

If you need to revert to the default firmware, you can find the hex files under 
the top level *firmware* folder in this repository.

## Hardware Specifications

- Raytac MDBT50 module with Nordic nRF52840 BLE/802.15.4 SoC
- 1.54 inch 200x200 pixel red/black/white epaper display - GDEW0154Z04
- CR2477 coin cell holder
- Micro USB (device) 
- RGB LED
- NFC (PCB) antenna
- Push button
- USB/Battery power switch
- Extra GPIOs
- SWD Programming header
- Mounting holes

## Datasheet and Schematic

You can find the schematic and data sheet in the links below.

- [Datasheet](https://gitlab.com/electronutlabs-public/papyr/raw/master/papyr_v0.3_datasheet.pdf?inline=false)
- [Schematic](https://gitlab.com/electronutlabs-public/papyr/raw/master/hardware/papyr_schematic_v_0_3.pdf?inline=false)

## Connections and IOs

The available pin headers on the papyr can be used by the users are described below. Please refer to our documentation on [zephyr](https://docs.zephyrproject.org/latest/boards/arm/nrf52840_papyr/doc/nrf52840_papyr.html) project's documentation to know more.

LED

    LED1 (green) = P0.13
    LED2 (blue) = P0.15
    LED3 (red) = P0.14

Push buttons

    Reset = SW0 = P0.18 (can be used as GPIO also)

UART

    TX = P0.8
    RX = P0.7

I2C

I2C pins connected to onboard sensors (I2C_0):

    SDA = P0.5
    SCL = P0.6

SPI

The e-paper display is connected to the chip via SPI on the following pins (SPI_1):

    SCK = P0.31
    MOSI = P0.29
    MISO = P1.1 (not used by the display)

NOTE: P1.1 is pin 33 in absolute enumeration.

Other pins used by the e-paper display are:

    E-ink enable = P0.11 (cuts off power to the display with MOSFET)
    CS = P0.30
    BUSY = P0.3
    D/C = P0.28
    RES = P0.2

## Papyr Dimensions

Dimensions for Papyr v0.3 board can be downloaded [here](https://gitlab.com/electronutlabs-public/papyr/raw/master/hardware/papyr_v0.3_dimensions.pdf?inline=false).

[![papyr dims](papyr-dims.png)](https://gitlab.com/electronutlabs-public/papyr/raw/master/hardware/papyr_v0.3_dimensions.pdf?inline=false)

## Current Consumption 

Papyr is designed for very low power applications. For the default fimrware that
ships with Papyr, when the device is advertising (not connected), the current
draw is about 22 uA.

## Code Repository

You can find all code and design files related to Papyr at the link below:

[https://gitlab.com/electronutlabs-public/papyr/](https://gitlab.com/electronutlabs-public/papyr/)

## Buy Papyr

Papyr is available for purchase from our [Tindie store](https://www.tindie.com/products/ElectronutLabs/papyr-nordic-nrf52840-epaper-display/). Please email us at **info@electronut.in** if you have any questions.

<a href="https://www.tindie.com/stores/ElectronutLabs/?ref=offsite_badges&utm_source=sellers_ElectronutLabs&utm_medium=badges&utm_campaign=badge_large"><img src="https://d2ss6ovg47m0r5.cloudfront.net/badges/tindie-larges.png" alt="I sell on Tindie" width="200" height="104"></a>

## About Electronut Labs

**Electronut Labs** is an Embedded Systems company based in Bangalore, India. More 
information at our [website](https://electronut.in).
